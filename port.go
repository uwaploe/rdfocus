package main

import (
	"io"
	"net"
	"time"

	"github.com/tarm/serial"
)

// Port represents a serial port interface
type Port interface {
	io.ReadWriteCloser
	CanChangeBaud() bool
	SetBaud(baudrate int) error
	EchoMode(state bool)
}

type serialPort struct {
	*serial.Port
	device   string
	echoMode bool
}

func SerialPort(device string, baud int) (*serialPort, error) {
	cfg := &serial.Config{
		Name:        device,
		Baud:        baud,
		ReadTimeout: time.Second * 5,
	}
	p, err := serial.OpenPort(cfg)
	if err != nil {
		return nil, err
	}

	return &serialPort{device: device, Port: p}, nil
}

func (s *serialPort) SetBaud(baud int) error {
	cfg := &serial.Config{
		Name:        s.device,
		Baud:        baud,
		ReadTimeout: time.Second * 5,
	}
	p, err := serial.OpenPort(cfg)
	if err != nil {
		return err
	}
	s.Port.Close()
	s.Port = p
	return nil
}

func (s *serialPort) CanChangeBaud() bool {
	return true
}

func (s *serialPort) EchoMode(state bool) {
	s.echoMode = state
}

func (s *serialPort) Write(p []byte) (int, error) {
	if !s.echoMode {
		return s.Port.Write(p)
	} else {
		resp := make([]byte, 1)
		for i := 0; i < len(p); i++ {
			s.Port.Write(p[i : i+1])
			_, err := s.Port.Read(resp)
			if err != nil {
				return 0, err
			}
		}
	}
	return len(p), nil
}

type networkPort struct {
	*net.TCPConn
	echoMode bool
}

func NetworkPort(address string) (*networkPort, error) {
	addr, err := net.ResolveTCPAddr("tcp4", address)
	if err != nil {
		return nil, err
	}
	conn, err := net.DialTCP("tcp", nil, addr)
	if err != nil {
		return nil, err
	}

	return &networkPort{TCPConn: conn}, nil
}

func (np *networkPort) SetBaud(baud int) error {
	return nil
}

func (np *networkPort) CanChangeBaud() bool {
	return false
}

func (np *networkPort) Read(p []byte) (int, error) {
	np.TCPConn.SetReadDeadline(time.Now().Add(time.Second * 5))
	return np.TCPConn.Read(p)
}

func (np *networkPort) EchoMode(state bool) {
	np.echoMode = state
}

func (np *networkPort) Write(p []byte) (int, error) {
	if !np.echoMode {
		return np.TCPConn.Write(p)
	} else {
		resp := make([]byte, 1)
		for i := 0; i < len(p); i++ {
			np.TCPConn.Write(p[i : i+1])
			_, err := np.TCPConn.Read(resp)
			if err != nil {
				return 0, err
			}
		}
	}
	return len(p), nil
}
