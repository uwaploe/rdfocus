// Rdfocus reads the PC Client String from the MacArtney FOCUS underwater
// vehicle and publishes the data to a NATS Streaming Server
package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	nmea "bitbucket.org/mfkenney/go-nmea"
	stan "github.com/nats-io/go-nats-streaming"
	"github.com/pkg/errors"
	"github.com/vmihailenco/msgpack"
)

var Version = "dev"
var BuildDate = "unknown"

type focusRecord struct {
	T    time.Time
	Data map[string]float32
}

func (f *focusRecord) fromNMEA(s *nmea.Sentence, names []string) error {
	for i, name := range names {
		x, err := strconv.ParseFloat(s.Fields[i], 32)
		if err != nil {
			return errors.Wrap(err, fmt.Sprintf("converting %q", s.Fields[i]))
		}
		f.Data[name] = float32(x)
	}
	return nil
}

func publishData(sc stan.Conn, subject string, t time.Time,
	s *nmea.Sentence, names []string) error {
	rec := focusRecord{T: t, Data: make(map[string]float32)}
	err := rec.fromNMEA(s, names)
	if err != nil {
		return errors.Wrap(err, "NMEA read")
	}
	b, err := msgpack.Marshal(&rec)
	if err != nil {
		return errors.Wrap(err, "Msgpack encode")
	}

	return sc.Publish(subject, b)
}

func main() {
	natsURL := os.Getenv("NATS_URL")
	clusterID := os.Getenv("NATS_CLUSTER_ID")
	focusDev := os.Getenv("FOCUS_DEVICE")
	subject := os.Getenv("FOCUS_SUBJECT")
	focusFields := strings.Split(os.Getenv("FOCUS_FIELDS"), ",")

	if focusDev == "" {
		log.Fatal("FOCUS device not specified, aborting")
	}

	if len(focusFields) == 0 {
		log.Fatal("FOCUS_FIELDS env variable not set")
	}

	sc, err := stan.Connect(clusterID, "focus-pub", stan.NatsURL(natsURL))
	if err != nil {
		log.Fatalf("Cannot connect: %v", err)
	}
	defer sc.Close()

	// FOCUS_DEVICE can either be a serial device name or an IPADDR:PORT
	// network address.
	var (
		p Port
	)

	if strings.Contains(focusDev, ":") {
		p, err = NetworkPort(focusDev)
	} else {
		baud, err := strconv.Atoi(os.Getenv("FOCUS_BAUD"))
		if err != nil {
			log.Fatalf("Cannot read FOCUS_BAUD env variable: %v", err)
		}
		p, err = SerialPort(focusDev, baud)
	}

	if err != nil {
		log.Fatalf("Cannot open FOCUS device: %v", err)
	}
	defer p.Close()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	log.Printf("Starting FOCUS monitor %s", Version)
	scanner := bufio.NewScanner(p)
loop:
	for scanner.Scan() {
		t := time.Now()
		s, err := nmea.ParseSentence(scanner.Bytes())
		if err != nil {
			log.Printf("NMEA parse error: %v", err)
		} else {
			err = publishData(sc, subject, t, s, focusFields)
			if err != nil {
				log.Printf("Publish failed: %v", err)
			}
		}

		select {
		case <-sigs:
			break loop
		default:
		}
	}

	if err = scanner.Err(); err != nil {
		log.Fatal(err)
	}

	log.Printf("done")
}
