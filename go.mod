module bitbucket.org/uwaploe/rdfocus

require (
	bitbucket.org/mfkenney/go-nmea v1.1.0
	github.com/gogo/protobuf v1.2.0 // indirect
	github.com/nats-io/go-nats v1.7.0 // indirect
	github.com/nats-io/go-nats-streaming v0.4.0
	github.com/nats-io/nkeys v0.0.2 // indirect
	github.com/nats-io/nuid v1.0.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	github.com/vmihailenco/msgpack v4.0.1+incompatible
	golang.org/x/sys v0.0.0-20190116161447-11f53e031339 // indirect
)
