ARG GO_VERSION=1.11.1

FROM golang:${GO_VERSION}-alpine AS builder

RUN apk add --no-cache ca-certificates git
WORKDIR /src
# Fetch dependencies
COPY ./go.mod ./go.sum ./
RUN go mod download

# Import the code from the context.
COPY ./ ./

ARG VERSION
ARG BUILD_DATE
ARG VCS_REF

# Build the executable to `/app`. Mark the build as statically linked.
RUN CGO_ENABLED=0 go build \
    -installsuffix 'static' \
    -ldflags "-s -X main.Version=${VERSION} -X main.BuildDate=${DATE}" \
    -o /app .

FROM alpine:latest
COPY --from=builder /app /app

ARG VERSION
ARG BUILD_DATE
ARG VCS_REF

LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.version=$VERSION \
      org.label-schema.vcs-ref=$VCS_REF

ENTRYPOINT ["/app"]
