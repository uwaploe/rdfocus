PACKAGE = rdfocus
DATE    ?= $(shell date +%FT%T%z)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
			cat $(CURDIR)/.version 2> /dev/null || echo v0)
VCS_REF := $(shell git log -1 --pretty=%h)
BIN      = $(CURDIR)/bin

GO := go1.11
IMAGE := rdfocus

.PHONY: all
all: $(BIN)
	@$(GO) build -v \
	  -tags release \
	  -ldflags '-s -X main.Version=$(VERSION) -X main.BuildDate=$(DATE)' \
	  -o $(shell realpath --relative-to=. $(BIN)/$(PACKAGE))

$(BIN):
	@mkdir -p $@

image: main.go
	docker build -t $(IMAGE):$(VERSION) -t $(IMAGE):latest \
	--build-arg VERSION="${VERSION}" \
	--build-arg BUILD_DATE="${DATE}" \
	--build-arg VCS_REF="${VCS_REF}" .

.PHONY: clean
clean:
	@rm -rf $(BIN)
